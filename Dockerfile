FROM golang:1.12

RUN apt update -y && apt install -y zip curl build-essential awscli jq gcc && \
  wget --quiet https://releases.hashicorp.com/terraform/0.12.28/terraform_0.12.28_linux_amd64.zip \
  && unzip terraform_0.12.28_linux_amd64.zip \
  && mv terraform /usr/bin \
  && rm terraform_0.12.28_linux_amd64.zip

ADD car-api/ .

CMD make build
